
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.List;

import org.junit.Test;

import com.google.gson.Gson;
import com.jayway.restassured.path.xml.XmlPath;

import compoundClassifications.model.CompoundClassifications;
import compoundClassifications.xml.model.ChebiClassification;
import compoundClassifications.xml.model.ExactMatch;
import compoundClassifications.xml.model.Root;
import compoundClassifications.xml.model.PrimaryTopic;
import compoundClassifications.xml.model.SingleMatch;
import compoundClassifications.xml.model.XmlResult;
import static com.jayway.restassured.path.xml.XmlPath.from;
import static org.fest.assertions.api.Assertions.assertThat;
import static compoundClassifications.mappers.ChebiClassificationMapper.toChebiClassification;
import static compoundClassifications.mappers.ChebiClassificationMapper.toChebiClassifications;
import static compoundClassifications.mappers.MarketMapper.toMarket;
import static compoundClassifications.mappers.ExactMatchMapper.toExactMatch;
import static compoundClassifications.mappers.SingleMatchMapper.toSingleMatches;
import static compoundClassifications.mappers.PrimaryTopicMapper.toPrimaryTopic;
import static compoundClassifications.mappers.XmlCompoundClassificationsResultMapper.toCompoundClassificationsResult;

public class OpenPhactsTest {

	@Test
    public void testJSONCompoundClassifications() throws Exception {
       
		BufferedReader br = new BufferedReader(
				new FileReader("file.json"));
    	
		CompoundClassifications compound = new Gson().fromJson(br, CompoundClassifications.class);

        assertThat(compound.getFormat()).isEqualTo("linked-data-api");
        assertThat(compound.getVersion()).isEqualTo("1.5");
        assertThat(compound.getResult().get_about()).isEqualTo("https://beta.openphacts.org/1.5/compound/classifications?uri=http%3A%2F%2Fwww.conceptwiki.org%2Fconcept%2F38932552-111f-4a4e-a46a-4ed1d7bdf9d5&app_id=d283dc2c&app_key=5b723e13d2b6d891edae0c2a8e1e85a0&tree=chebi");

    }
	
	@Test
    public void testXMLCompoundClassifications() throws Exception {
       
		 XmlPath xmlPath = from(
	                "<xml>" +
	                 "<result format=\"linked-data-api\" version=\"1.5\" href=\"https://beta.openphacts.org/1.5/compound/classifications?uri=http%3A%2F%2Fwww.conceptwiki.org%2Fconcept%2F38932552-111f-4a4e-a46a-4ed1d7bdf9d5&amp;app_id=d283dc2c&amp;app_key=5b723e13d2b6d891edae0c2a8e1e85a0&amp;tree=chebi&amp;_format=xml\">"+
	                 "<activeLens>Default</activeLens>"+
	                 "<linkPredicate href=\"http://www.w3.org/2004/02/skos/core#exactMatch\"/>"+
	                 "<extendedMetadataVersion href=\"https://beta.openphacts.org/1.5/compound/classifications?uri=http%3A%2F%2Fwww.conceptwiki.org%2Fconcept%2F38932552-111f-4a4e-a46a-4ed1d7bdf9d5&amp;app_id=d283dc2c&amp;app_key=5b723e13d2b6d891edae0c2a8e1e85a0&amp;tree=chebi&amp;_format=xml&amp;_metadata=all%2Cviews%2Cformats%2Cexecution%2Cbindings%2Csite\"/>"+
	                 "<definition href=\"https://beta.openphacts.org/api-config\"/>"+
	                 "</result>"+
	                 "</xml>");    	
	        
		 
		 	Root compound = toMarket(xmlPath.getNode("xml").children().get(0));

	        assertThat(compound.getFormat()).isEqualTo("linked-data-api");
	        assertThat(compound.getVersion()).isEqualTo("1.5");
	        assertThat(compound.getResult().getActiveLens()).isEqualTo("Default");
	        assertThat(compound.getResult().getDefinition()).isEqualTo("https://beta.openphacts.org/api-config");
	        assertThat(compound.getResult().getLinkPredicate()).isEqualTo("http://www.w3.org/2004/02/skos/core#exactMatch");

	        // check data's encoding
	        //assertThat(compound.getResult().getExtendedMetadataVersion()).isEqualTo("https://beta.openphacts.org/1.5/compound/classifications?uri=http%3A%2F%2Fwww.conceptwiki.org%2Fconcept%2F38932552-111f-4a4e-a46a-4ed1d7bdf9d5&amp;app_id=d283dc2c&amp;app_key=5b723e13d2b6d891edae0c2a8e1e85a0&amp;tree=chebi&amp;_format=xml&amp;_metadata=all%2Cviews%2Cformats%2Cexecution%2Cbindings%2Csite");

    }
	
	
	@Test
    public void testhasChebiClassification() throws Exception {
       
		 XmlPath xmlPath = from(
	                "<xml>" +
	                "<item href=\"http://purl.obolibrary.org/obo/CHEBI_38637\">"+
	                "<prefLabel datatype=\"string\">tyrosine kinase inhibitor</prefLabel>"+
	                "<classificationType href=\"http://purl.obolibrary.org/obo#has_role\">"+
	                		"<prefLabel datatype=\"string\">has role</prefLabel>"+
	                "</classificationType>"+
	                "<inDataset href=\"http://www.ebi.ac.uk/chebi\">"+
	                	"<prefLabel>ChEBI Ontology</prefLabel>"+
	                "</inDataset>"+
	                "</item>"
	                	+"</xml>");    	
	        
		 ChebiClassification chebi = toChebiClassification(xmlPath.getNode("xml").children().get(0));

	     assertThat(chebi.getAbout()).isEqualTo("http://purl.obolibrary.org/obo/CHEBI_38637");
	     assertThat(chebi.getinDataset().getAbout()).isEqualTo("http://www.ebi.ac.uk/chebi");
	     assertThat(chebi.getinDataset().getPrefLabel()).isEqualTo("ChEBI Ontology");
	     assertThat(chebi.getclassificationType().getAbout()).isEqualTo("http://purl.obolibrary.org/obo#has_role");
	     assertThat(chebi.getclassificationType().getPrefLabel()).isEqualTo("has role");


    }
	
	@Test
    public void testhasChebiClassifications() throws Exception {
       
		 XmlPath xmlPath = from(
	                "<xml>" +
	                "<hasChebiClassification>"+
	                "<item href=\"http://purl.obolibrary.org/obo/CHEBI_38637\">"+
	                "<prefLabel datatype=\"string\">tyrosine kinase inhibitor</prefLabel>"+
	                "<classificationType href=\"http://purl.obolibrary.org/obo#has_role\">"+
	                		"<prefLabel datatype=\"string\">has role</prefLabel>"+
	                "</classificationType>"+
	                "<inDataset href=\"http://www.ebi.ac.uk/chebi\">"+
	                	"<prefLabel>ChEBI Ontology</prefLabel>"+
	                "</inDataset>"+
	                "</item>"+
	                "<item href=\"http://purl.obolibrary.org/obo/CHEBI_83565\">"+
	                "<prefLabel datatype=\"string\">(trifluoromethyl)benzenes</prefLabel>"+
	                "<classificationType href=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#type\">"+
	                 "<prefLabel>Type</prefLabel>"+
	                "</classificationType>"+
	                "<inDataset href=\"http://www.ebi.ac.uk/chebi\">"+
	                  "<prefLabel>ChEBI Ontology</prefLabel>"+
	                "</inDataset>"+
	              "</item>"+
	                "</hasChebiClassification>"+
				 "</xml>");    	
	        
		 
		 
		 
		 List<ChebiClassification> chebis = toChebiClassifications(xmlPath.getNode("xml").getNode("hasChebiClassification").children());

	     assertThat(chebis).hasSize(2);
	     assertThat(chebis.get(0).getAbout()).isEqualTo("http://purl.obolibrary.org/obo/CHEBI_38637");
	     assertThat(chebis.get(1).getAbout()).isEqualTo("http://purl.obolibrary.org/obo/CHEBI_83565");
	     

    }
	
	@Test
    public void testSingleMatch() throws Exception {
       
		 XmlPath xmlPath = from(
	                "<xml>" +
	                "<exactMatch>"+
	                "<item href=\"http://rdf.ebi.ac.uk/resource/chembl/molecule/CHEMBL1336\">"+
	                "<hasChebiClassification>"+
	                "<item href=\"http://purl.obolibrary.org/obo/CHEBI_38637\">"+
	                "<prefLabel datatype=\"string\">tyrosine kinase inhibitor</prefLabel>"+
	                "<classificationType href=\"http://purl.obolibrary.org/obo#has_role\">"+
	                		"<prefLabel datatype=\"string\">has role</prefLabel>"+
	                "</classificationType>"+
	                "<inDataset href=\"http://www.ebi.ac.uk/chebi\">"+
	                	"<prefLabel>ChEBI Ontology</prefLabel>"+
	                "</inDataset>"+
	                "</item>"+
	                "<item href=\"http://purl.obolibrary.org/obo/CHEBI_83565\">"+
	                "<prefLabel datatype=\"string\">(trifluoromethyl)benzenes</prefLabel>"+
	                "<classificationType href=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#type\">"+
	                 "<prefLabel>Type</prefLabel>"+
	                "</classificationType>"+
	                "<inDataset href=\"http://www.ebi.ac.uk/chebi\">"+
	                  "<prefLabel>ChEBI Ontology</prefLabel>"+
	                "</inDataset>"+
	              "</item>"+
	                "</hasChebiClassification>"+
	               "<inDataset href=\"http://www.ebi.ac.uk/chembl\"/>"+
	                "</item>"+
	               "<item href=\"http://www.conceptwiki.org/concept/38932552-111f-4a4e-a46a-4ed1d7bdf9d5\"/>"+
	              "</exactMatch>"+
				 "</xml>");    	
	        
		 				 
		List<SingleMatch> single = toSingleMatches(xmlPath.getNode("xml").getNode("exactMatch").children());
	     assertThat(single).hasSize(2);
	     assertThat(single.get(0).getAbout()).isEqualTo("http://rdf.ebi.ac.uk/resource/chembl/molecule/CHEMBL1336");
	     assertThat(single.get(1).getAbout()).isEqualTo("http://www.conceptwiki.org/concept/38932552-111f-4a4e-a46a-4ed1d7bdf9d5");

	     ExactMatch match = toExactMatch(xmlPath.getNode("xml").getNode("exactMatch"));
	     assertThat(match.getSingleMatch()).hasSize(2);
	     assertThat(match.getSingleMatch().get(0).getAbout()).isEqualTo("http://rdf.ebi.ac.uk/resource/chembl/molecule/CHEMBL1336");;

    }
	
	@Test
    public void testPrimaryTopic() throws Exception {
		 XmlPath xmlPath = from(
	                "<xml>" +
	                "<primaryTopic href=\"http://www.conceptwiki.org/concept/38932552-111f-4a4e-a46a-4ed1d7bdf9d5\">"+
	                "<prefLabel xml:lang=\"en\">Sorafenib</prefLabel>"+
	                "<exactMatch>"+
	                "<item href=\"http://rdf.ebi.ac.uk/resource/chembl/molecule/CHEMBL1336\">"+
	                "<hasChebiClassification>"+
	                "<item href=\"http://purl.obolibrary.org/obo/CHEBI_38637\">"+
	                "<prefLabel datatype=\"string\">tyrosine kinase inhibitor</prefLabel>"+
	                "<classificationType href=\"http://purl.obolibrary.org/obo#has_role\">"+
	                		"<prefLabel datatype=\"string\">has role</prefLabel>"+
	                "</classificationType>"+
	                "<inDataset href=\"http://www.ebi.ac.uk/chebi\">"+
	                	"<prefLabel>ChEBI Ontology</prefLabel>"+
	                "</inDataset>"+
	                "</item>"+
	                "<item href=\"http://purl.obolibrary.org/obo/CHEBI_83565\">"+
	                "<prefLabel datatype=\"string\">(trifluoromethyl)benzenes</prefLabel>"+
	                "<classificationType href=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#type\">"+
	                 "<prefLabel>Type</prefLabel>"+
	                "</classificationType>"+
	                "<inDataset href=\"http://www.ebi.ac.uk/chebi\">"+
	                  "<prefLabel>ChEBI Ontology</prefLabel>"+
	                "</inDataset>"+
	              "</item>"+
	                "</hasChebiClassification>"+
	               "<inDataset href=\"http://www.ebi.ac.uk/chembl\"/>"+
	                "</item>"+
	               "<item href=\"http://www.conceptwiki.org/concept/38932552-111f-4a4e-a46a-4ed1d7bdf9d5\"/>"+
	              "</exactMatch>"+
	               "<inDataset href=\"http://www.conceptwiki.org\"/>"+
	               "</primaryTopic>"+
				 "</xml>");    	
		 
	     PrimaryTopic topic = toPrimaryTopic(xmlPath.getNode("xml").getNode("primaryTopic"));

	     assertThat(topic.getExactMatch().getSingleMatch()).hasSize(2);
	     assertThat(topic.getExactMatch().getSingleMatch().get(0).getAbout()).isEqualTo("http://rdf.ebi.ac.uk/resource/chembl/molecule/CHEMBL1336");
	}
	
	@Test
    public void testResult() throws Exception {
		
		
		 XmlPath xmlPath = from(
	                "<xml>" +
	   	            "<result format=\"linked-data-api\" version=\"1.5\" href=\"https://beta.openphacts.org/1.5/compound/classifications?uri=http%3A%2F%2Fwww.conceptwiki.org%2Fconcept%2F38932552-111f-4a4e-a46a-4ed1d7bdf9d5&amp;app_id=d283dc2c&amp;app_key=5b723e13d2b6d891edae0c2a8e1e85a0&amp;tree=chebi&amp;_format=xml\">"+
		            "<primaryTopic href=\"http://www.conceptwiki.org/concept/38932552-111f-4a4e-a46a-4ed1d7bdf9d5\">"+
	                "<prefLabel xml:lang=\"en\">Sorafenib</prefLabel>"+
	                "<exactMatch>"+
	                "<item href=\"http://rdf.ebi.ac.uk/resource/chembl/molecule/CHEMBL1336\">"+
	                "<hasChebiClassification>"+
	                "<item href=\"http://purl.obolibrary.org/obo/CHEBI_38637\">"+
	                "<prefLabel datatype=\"string\">tyrosine kinase inhibitor</prefLabel>"+
	                "<classificationType href=\"http://purl.obolibrary.org/obo#has_role\">"+
	                		"<prefLabel datatype=\"string\">has role</prefLabel>"+
	                "</classificationType>"+
	                "<inDataset href=\"http://www.ebi.ac.uk/chebi\">"+
	                	"<prefLabel>ChEBI Ontology</prefLabel>"+
	                "</inDataset>"+
	                "</item>"+
	                "<item href=\"http://purl.obolibrary.org/obo/CHEBI_83565\">"+
	                "<prefLabel datatype=\"string\">(trifluoromethyl)benzenes</prefLabel>"+
	                "<classificationType href=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#type\">"+
	                 "<prefLabel>Type</prefLabel>"+
	                "</classificationType>"+
	                "<inDataset href=\"http://www.ebi.ac.uk/chebi\">"+
	                  "<prefLabel>ChEBI Ontology</prefLabel>"+
	                "</inDataset>"+
	              "</item>"+
	                "</hasChebiClassification>"+
	               "<inDataset href=\"http://www.ebi.ac.uk/chembl\"/>"+
	                "</item>"+
	               "<item href=\"http://www.conceptwiki.org/concept/38932552-111f-4a4e-a46a-4ed1d7bdf9d5\"/>"+
	              "</exactMatch>"+
	               "<inDataset href=\"http://www.conceptwiki.org\"/>"+
	               "</primaryTopic>"+
	               "<activeLens>Default</activeLens>"+
		            "<linkPredicate href=\"http://www.w3.org/2004/02/skos/core#exactMatch\"/>"+
		            "<extendedMetadataVersion href=\"https://beta.openphacts.org/1.5/compound/classifications?uri=http%3A%2F%2Fwww.conceptwiki.org%2Fconcept%2F38932552-111f-4a4e-a46a-4ed1d7bdf9d5&amp;app_id=d283dc2c&amp;app_key=5b723e13d2b6d891edae0c2a8e1e85a0&amp;tree=chebi&amp;_format=xml&amp;_metadata=all%2Cviews%2Cformats%2Cexecution%2Cbindings%2Csite\"/>"+
		            "<definition href=\"https://beta.openphacts.org/api-config\"/>"+
		            "</result>"+
				 "</xml>");    	
		 
	     XmlResult result = toCompoundClassificationsResult(xmlPath.getNode("xml").getNode("result"));
	     assertThat(result.getPrimaryTopic().getExactMatch().getSingleMatch()).hasSize(2);

		 	Root compound = toMarket(xmlPath.getNode("xml").children().get(0));

	        assertThat(compound.getFormat()).isEqualTo("linked-data-api");
	        assertThat(compound.getVersion()).isEqualTo("1.5");
	        assertThat(compound.getResult().getActiveLens()).isEqualTo("Default");
	        assertThat(compound.getResult().getDefinition()).isEqualTo("https://beta.openphacts.org/api-config");
	        assertThat(compound.getResult().getLinkPredicate()).isEqualTo("http://www.w3.org/2004/02/skos/core#exactMatch");
		    assertThat(compound.getResult().getPrimaryTopic().getExactMatch().getSingleMatch()).hasSize(2);

	}

}
