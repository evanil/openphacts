package compoundClassifications.xml.model;

public interface ClassificationType {

	String getAbout();
	String getPrefLabel();

}
