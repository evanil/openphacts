package compoundClassifications.xml.model;

public interface XmlResult {

	 String getDefinition();
	 String getExtendedMetadataVersion();
	 String getLinkPredicate() ;
	 String getActiveLens();
	 PrimaryTopic getPrimaryTopic();
}
