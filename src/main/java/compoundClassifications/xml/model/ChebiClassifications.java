package compoundClassifications.xml.model;

import java.util.List;

import com.jayway.restassured.path.xml.element.Node;

import compoundClassifications.mappers.ClassificationTypeMapper;
import compoundClassifications.mappers.InDataSetMapper;

public class ChebiClassifications {

	
	private ChebiClassifications() {
	    
	}

    public static ChebiClassification newChebiClassification(Node node) {
            
    	
    	 return new ChebiClassificationImpl(node.attributes().get("href"), InDataSetMapper.toInDataSet(node.getNode("inDataset")), ClassificationTypeMapper.toClassificationType(node.getNode("classificationType")));    	
    	
    }
    
    private static class ChebiClassificationImpl implements ChebiClassification {

    	private final String about;
    	private final InDataSet inDataset;
    	private final ClassificationType classificationType;

        private ChebiClassificationImpl(String about, InDataSet inDataset, ClassificationType classificationType) {
        	this.about=about;
        	this.inDataset=inDataset;
        	this.classificationType = classificationType;
        }

		@Override
		public String getAbout() {
			return this.about;
		}

		@Override
		public InDataSet getinDataset() {
			return this.inDataset;
		}

		@Override
		public ClassificationType getclassificationType() {
			return this.classificationType;
		}

		@Override
		public String getPrefLabel() {
			// TODO Auto-generated method stub
			return null;
		}
    }
}
