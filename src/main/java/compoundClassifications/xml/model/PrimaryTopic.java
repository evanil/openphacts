package compoundClassifications.xml.model;

public interface PrimaryTopic {

	 String getAbout();
	 String getPrefLabel();
	 ExactMatch getExactMatch();
	 String getinDataset();
	 
}
