package compoundClassifications.xml.model;


import java.util.Map;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.jayway.restassured.path.xml.element.Node;

public class ClassificationTypes  {

    private ClassificationTypes() {
    }

    public static ClassificationType newClassificationType(Node node) {
            	
    	return new ClassificationTypeImpl(node.attributes().get("href"),node.getNode("prefLabel").toString());
    	
    }
    
    private static class ClassificationTypeImpl implements ClassificationType {

    	private final String about;
    	private final String prefLabel;


        private ClassificationTypeImpl(String about, String prefLabel) {
        	this.about=about;
        	this.prefLabel=prefLabel;
        }

        @Override
        public boolean equals(Object obj) {
            return EqualsBuilder.reflectionEquals(this, obj);
        }

        @Override
        public int hashCode() {
            return HashCodeBuilder.reflectionHashCode(this);
        }
        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this);
        }


		@Override
		public String getAbout() {
			return this.about;
		}


		@Override
		public String getPrefLabel() {
			return this.prefLabel;
		}

		
    }

	
    
}
