package compoundClassifications.xml.model;

import java.util.List;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import static compoundClassifications.mappers.ChebiClassificationMapper.toChebiClassifications;
import static compoundClassifications.mappers.SingleMatchMapper.toSingleMatches;

import com.jayway.restassured.path.xml.element.Node;


public class ExactMatches {

		private ExactMatches() {
	    
		}

	    public static ExactMatch newExactMatch(Node node) {
        	
	    	return new ExactMatchImpl(toSingleMatches(node.children()));
	    	
	    }
	    
	    private static class ExactMatchImpl implements ExactMatch {

	    	private final List<SingleMatch> single;


	        private ExactMatchImpl(List<SingleMatch> single) {
	        	this.single=single;
	        }

	        @Override
	        public boolean equals(Object obj) {
	            return EqualsBuilder.reflectionEquals(this, obj);
	        }

	        @Override
	        public int hashCode() {
	            return HashCodeBuilder.reflectionHashCode(this);
	        }
	        @Override
	        public String toString() {
	            return ToStringBuilder.reflectionToString(this);
	        }

			@Override
			public List<SingleMatch> getSingleMatch() {
				return this.single;
			}




			
	    }
	 	    
}
