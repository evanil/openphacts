package compoundClassifications.xml.model;

public interface ChebiClassification {

	String getAbout();
	InDataSet getinDataset();
	ClassificationType getclassificationType();
	String getPrefLabel();
	
}
