package compoundClassifications.xml.model;

import java.util.List;

import com.jayway.restassured.path.xml.element.Node;

import static compoundClassifications.mappers.ChebiClassificationMapper.toChebiClassifications;

public class SingleMatches  {

	private SingleMatches() {
	    
	}

	  public static SingleMatch newSingleMatch(Node node) {
      		    	
		  if(node.getNode("hasChebiClassification")!=null)
			  return new SingleMatcheImpl(node.attributes().get("href"), toChebiClassifications(node.getNode("hasChebiClassification").children()));
		  else
			   return new SingleMatcheImpl(node.attributes().get("href"), null);
	  
	    }
	    
	    private static class SingleMatcheImpl implements SingleMatch {

	    	private final String about;
	    	private final List<ChebiClassification> chebiClassification;
	    	
	    	private SingleMatcheImpl(String about, List<ChebiClassification> chebiClassification) {
	        	this.about=about;
	        	this.chebiClassification=chebiClassification;
	        }
	    	
			@Override
			public String getAbout() {
				return this.about;
			}

			@Override
			public List<ChebiClassification> getChebiClassification() {
				return this.chebiClassification;
			}

	    	
	    }
	
	
}
