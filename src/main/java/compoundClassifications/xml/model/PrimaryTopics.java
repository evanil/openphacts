package compoundClassifications.xml.model;

import static compoundClassifications.mappers.ExactMatchMapper.toExactMatch;
import static compoundClassifications.mappers.XmlCompoundClassificationsResultMapper.toCompoundClassificationsResult;

import java.util.Map;

import com.jayway.restassured.path.xml.element.Node;

public class PrimaryTopics  {

	 	private PrimaryTopics() {
	    }

	    public static PrimaryTopic newPrimaryTopic(Map<String,String> attributes, Node node) {
	            	
	    	return new PrimaryTopicImpl(attributes.get("href"),
	    			                    attributes.get("prefLabel"),
	    			                    attributes.get("inDataset"),
	    			                    toExactMatch(node.getNode("exactMatch")));
	    }

	    private static class PrimaryTopicImpl implements PrimaryTopic {
	    	
	    	private final String about;
	    	private final String prefLabel;
	    	private final String inDataset;
	    	private final ExactMatch exactMatch;

	        private PrimaryTopicImpl(String about, String prefLabel, String inDataset, ExactMatch exactMatch) {
	        	this.about=about;
	        	this.prefLabel=prefLabel;
	        	this.inDataset=inDataset;
	        	this.exactMatch =exactMatch;        	
	        }

	    
	    	@Override
	    	public String getAbout() {
	    		return this.about;
	    	}

	    	@Override
	    	public String getPrefLabel() {
	    		return this.prefLabel;
	    	}
	    	
	    	@Override
	       public ExactMatch getExactMatch() {
	    		return this.exactMatch;
	    	}

	    	@Override
	    	public String getinDataset() {
	    		return this.inDataset;
	    	}

	    }
}
