package compoundClassifications.xml.model;

import org.joda.time.DateTime;

import compoundClassifications.model.CompoundClassificationsResult;

import java.util.List;

public interface Root {

	String getFormat();
    String getVersion();
    XmlResult getResult();

}
