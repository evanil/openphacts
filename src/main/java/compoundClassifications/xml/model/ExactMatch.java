package compoundClassifications.xml.model;

import java.util.List;

public interface ExactMatch {

	List<SingleMatch> getSingleMatch();
	
}
