package compoundClassifications.xml.model;

import java.util.List;

public interface SingleMatch {

	String getAbout();
	List<ChebiClassification> getChebiClassification();
}
