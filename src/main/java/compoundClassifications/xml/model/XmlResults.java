package compoundClassifications.xml.model;

import java.util.Map;

import com.jayway.restassured.path.xml.element.Node;
import static compoundClassifications.mappers.PrimaryTopicMapper.toPrimaryTopic;


public class XmlResults {

	
	 private XmlResults() {
	    }


	    public static XmlResult newXmlCompoundClassificationsResult(Node node) {

	    	return   new XmlCompoundClassificationsResultImpl(
	    		    			node.getNode("definition").attributes().get("href"), 
	    		    			node.getNode("extendedMetadataVersion").attributes().get("href"), 
	    		    			node.getNode("linkPredicate").attributes().get("href"),
	    		    			node.getNode("activeLens").toString(),
	    		    			toPrimaryTopic(node.getNode("primaryTopic"))
	    		    			);
	    	
	    }



		private static class XmlCompoundClassificationsResultImpl implements XmlResult {

	    	private final String definition;
	    	private final String extendedMetadataVersion;
	    	private final String linkPredicate;
	    	private final String activeLens;
	    	private final PrimaryTopic primaryTopic;

	        private XmlCompoundClassificationsResultImpl(String definition, String extendedMetadataVersion, String linkPredicate, String activeLens,PrimaryTopic topic ) {

	        	 this.definition= definition;
	        	 this.extendedMetadataVersion=extendedMetadataVersion; 
	        	 this.linkPredicate=linkPredicate;
	        	 this.activeLens= activeLens;
	        	 this.primaryTopic=topic;
	        }

			
			@Override
			public String getDefinition() {
				return this.definition;
			}

			@Override
			public String getExtendedMetadataVersion() {
				return this.extendedMetadataVersion;
			}

			@Override
			public String getLinkPredicate() {
				return this.linkPredicate;
			}

			@Override
			public String getActiveLens() {
				return this.activeLens;
			}


			@Override
			public PrimaryTopic getPrimaryTopic() {
				return this.primaryTopic;
			}


	    }
}
