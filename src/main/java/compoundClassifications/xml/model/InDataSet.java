package compoundClassifications.xml.model;

public interface InDataSet {

	String getAbout();
	String getPrefLabel();
	
}
