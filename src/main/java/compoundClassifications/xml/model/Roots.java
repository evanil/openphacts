package compoundClassifications.xml.model;

import java.util.Map;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.jayway.restassured.path.xml.element.Node;
import com.jayway.restassured.path.xml.element.NodeChildren;

import compoundClassifications.model.CompoundClassificationsResult;
import static compoundClassifications.mappers.XmlCompoundClassificationsResultMapper.toCompoundClassificationsResult;

public class Roots {

    private Roots() {
    }

    public static Root newMarket(Map<String,String> attributes, Node node) {
            	
    	return new MarketImpl(attributes.get("format"), attributes.get("version"), attributes.get("href"), toCompoundClassificationsResult(node) );
    }

    private static class MarketImpl implements Root {

    	private final XmlResult result;
    	private final String format;
    	private final String version;
    	private final String href;


        private MarketImpl(String format, String version, String href, XmlResult result) {
        	this.format=format;
        	this.version=version;
        	this.href=href;
        	this.result =result;        	
        }


        @Override
        public boolean equals(Object obj) {
            return EqualsBuilder.reflectionEquals(this, obj);
        }

        @Override
        public int hashCode() {
            return HashCodeBuilder.reflectionHashCode(this);
        }
        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this);
        }


		public XmlResult getResult() {
			
			return this.result;
			
		}

		@Override
		public String getFormat() {
			return this.format;
		}


		@Override
		public String getVersion() {
			return this.version;
		}

    }
}
